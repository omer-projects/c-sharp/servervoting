﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using System.Threading;
using VoteDLL;

namespace ServerVoting
{
    class Program
    {
        //Server
        static Server server;
        static string Password = "123";
        
        //Files
        static string UrlImages = @"\Images";
        static string UrlSave = @"\Save";
        static string[] UrlSaveLists = { @"\List_1.txt", @"\List_2.txt", @"\List_3.txt" };
        static string UrlItems = @"\Items.txt";
        static string UrlKeysList = @"\Keys List.txt";

        //Data
        static ByteArray ItemsAll;
        static List<ItemData> Items;
        static List<string> Keys;
        static string[] KeysStart;
        static bool CanPrint = true;

        //Open Server
        static void Main(string[] args)
        {
            Console.Title = "Load Voting";
            if (!LoadFiles())
            {
                Console.WriteLine("Is SetUp now!");
                Console.ReadKey();
                return;
            }
            Console.Title = "Server Voting";
            Console.WriteLine("Server Started...");
            ByteArray.Length = Net.MaxByte;
            ReadItems();
            KeysStart = File.ReadAllLines(UrlKeysList);
            Keys = KeysStart.ToList();
            GetPassword();
            //End
            Console.Clear();
            Console.WriteLine("Server Open");
            Console.WriteLine("IP: " + Net.NameToIP(Net.NameComputer));
            Console.WriteLine("Password: " + Password);
            server = new Server(Net.Port, Net.MaxByte);
            server.Read = Read;
            Thread thread = new Thread(GetCommends);
            thread.Start();
            server.Open();

        }
        static void GetPassword()
        {
            Console.Write("Password: ");
            Password = Console.ReadLine();
            Console.Clear();
        }

        //Server
        static void Read(object sender, byte[] array)
        {
            Chat chat = (Chat)sender;
            ByteArray command = new ByteArray(array);

            char hard = command.ReadChar();
            switch (hard)
            {
                case 'C'://Connect
                    {
                        string Password = command.ReadString();
                        ByteArray byteArray = new ByteArray();
                        byteArray.Write('C');
                        byteArray.Write((Program.Password == Password) ? 'T' : 'F');
                        chat.Write(byteArray.Values);
                    }
                    break;
                case 'E'://Exit (Disconnenc)
                    {
                        chat.Stop();
                    }
                    break;
                case 'A'://All data
                    {
                        chat.Write(ItemsAll.Values);
                    }
                    break;
                case 'P'://add Point
                    {
                        string key = command.ReadString();
                        int length = command.ReadInt();
                        string[] ids = new string[length];
                        for (int i = 0; i < length; i++)
                        {
                            ids[i] = command.ReadString();
                        }
                        ByteArray ChatSand = new ByteArray();
                        ChatSand.Write('K');
                        if (Keys.IndexOf(key) != -1)
                        {
                            ChatSand.Write('T');
                            Keys.Remove(key);
                            for (int i = 0; i < Items.Count; i++)
                            {
                                if (Array.IndexOf(ids, Items[i].ID) != -1)
                                {
                                    Items[i].AddPoint(1);
                                    if (CanPrint)
                                        Console.WriteLine("Add point now to -> " + Items[i].Name + " (Points: " + Items[i].Points + ")");
                                }
                            }
                            ByteArray byteArray = new ByteArray();
                            byteArray.Write('P');
                            byteArray.Write(length);
                            for (int i = 0; i < length; i++)
                            {
                                byteArray.Write(ids[i]);
                            }
                            server.Write(byteArray.Values);
                            
                        }
                        else
                        {
                            ChatSand.Write('F');
                            ChatSand.Write((Array.IndexOf(KeysStart, key) != -1) ? 'T' : 'F');
                        }
                        chat.Write(ChatSand.Values);
                    }
                    break;
            }
        }

        //File
        static bool LoadFiles()
        {
            bool chek = true;
            string Folder = Application.StartupPath;
            UrlImages = Folder + UrlImages;
            UrlSave = Folder + UrlSave;
            UrlItems = Folder + UrlItems;
            UrlKeysList = Folder + UrlKeysList;
            if (!Directory.Exists(UrlImages))
            {
                chek = false;
                Directory.CreateDirectory(UrlImages);
            }
            if (!Directory.Exists(UrlSave))
            {
                chek = false;
                Directory.CreateDirectory(UrlSave);
            }
            if (!File.Exists(UrlItems))
            {
                chek = false;
                File.WriteAllText(UrlItems, "Type:ID:Name:ImageName");
            }
            if (!File.Exists(UrlKeysList))
            {
                chek = false;
                File.WriteAllText(UrlKeysList, "In this file you nead to add all the Keys!!!");
            }
            if (!chek)
            {
                Bitmap ImageName = new Bitmap(100, 120);
                Graphics g = Graphics.FromImage(ImageName);
                g.Clear(Color.White);
                ImageName.Save(UrlImages + @"\ImageName.png");
            }
            return chek;
        }
        static void ReadItems()
        {
            ItemsAll = new ByteArray();
            ItemsAll.Write('D');
            Items = new List<ItemData>();
            string[] ItemsTxt = File.ReadAllLines(UrlItems);
            ItemsAll.Write(ItemsTxt.Length);
            foreach (var line in ItemsTxt)
            {
                ItemData add = new ItemData(line);
                ItemsAll.Write(add.Project);
                ItemsAll.Write(add.ID);
                ItemsAll.Write(add.Name);
                ItemsAll.Write(add.Image);
                if (add.Image == "")
                {
                    add.ImageBytes = new byte[0][];
                    ItemsAll.Write(0);
                }
                else
                {
                    string url = UrlImages + "\\" + add.Image;
                    if (Directory.Exists(url))
                    {
                        string[] files = Directory.GetFiles(url);
                        add.ImageBytes = new byte[files.Length][];
                        ItemsAll.Write(files.Length);
                        for (int i = 0; i < files.Length; i++)
                        {
                            add.ImageBytes[i] = File.ReadAllBytes(files[i]);
                            ItemsAll.Write(add.ImageBytes[i], true);
                        }
                    }
                    else
                    {
                        add.ImageBytes = new byte[1][];
                        add.ImageBytes[0] = File.ReadAllBytes(url + ".png");
                        ItemsAll.Write(1);
                        ItemsAll.Write(add.ImageBytes[0], true);
                    }
                }
                ItemsAll.Write(add.Points);
                Items.Add(add);
            }
        }

        //Commend
        static void GetCommends()
        {
            while (true)
            {
                string commend = Console.ReadLine();
                switch (commend.ToLower())
                {
                    case "help":
                        {
                            string[] commends = {
                                "Halp - Print the \"Commend List\".",
                                "Save - Save the points in folder.",
                                "Exit - Stop the server and exit all the voters.",
                                "Print (or Data) - Print all the data."
                            };
                            Console.WriteLine("List Commends:");
                            foreach (var line in commends)
                            {
                                Console.WriteLine(line);
                            }
                        }
                        break;
                    case "save":
                        {
                            List<string>[] Lines = new List<string>[3];
                            for (int i = 0; i < 3; i++)
                            {
                                Lines[i] = new List<string>();
                            }
                            for (int i = 0; i < Items.Count; i++)
                            {
                                var item = Items[i];
                                Lines[item.Project].Add("Points: " + item.Points + " -> " + item.ID + " - " + item.Name);
                            }
                            var time = DateTime.Now.TimeOfDay;
                            string folder = UrlSave + "\\" + time.Hours + "," + time.Minutes + "," + time.Seconds;
                            Directory.CreateDirectory(folder);
                            for (int i = 0; i < 3; i++)
                            {
                                string url = folder + UrlSaveLists[i];
                                File.AppendAllLines(url, Lines[i]);
                            }
                        }
                        break;
                    case "exit":
                        {
                            ByteArray e = new ByteArray();
                            e.Write('E');
                            foreach (var item in server.ListChat)
                            {
                                item.Write(e.Values);
                            }
                        }
                        break;
                    case "print":
                    case "data":
                        {
                            CanPrint = false;
                            var time = DateTime.Now.TimeOfDay;
                            Console.WriteLine("Time now: {0} : {1} : {2}", time.Hours, time.Minutes, time.Seconds);
                            Console.WriteLine("Voters in server: " + server.ListChat.Count);
                            Console.WriteLine("Key list: " + Keys.Count + " From " + KeysStart.Length);
                            Console.WriteLine("Password: " + Password);
                            Console.WriteLine("The Items:");
                            foreach (var item in Items)
                            {
                                Console.WriteLine(">\tPoints: " + item.Points + " -> " + item.ID + " - " + item.Name);
                            }
                            CanPrint = true;
                        }
                        break;
                }
            }
        }
    }
}
