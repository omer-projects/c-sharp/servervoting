﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoteDLL
{
    public class ByteArray
    {
        static public int Length = 0;

        public int IndexRead = 0, IndexWrite = 0;
        public int IndexBoolRead = 0, IndexBoolWrite = 0;
        public byte[] Values;

        public ByteArray()
        {
            Values = new byte[Length];
        }
        public ByteArray(byte[] Values)
        {
            this.Values = new byte[Values.Length];
            Array.Copy(Values, this.Values, Values.Length);
        }

        public void Write(byte value)
        {
            Values[IndexWrite] = value;
            IndexWrite++;
            IndexBoolWrite = 0;
        }
        public void Write(byte[] array, bool HaveLength)
        {
            if (HaveLength)
            {
                Write(array.Length);
            }
            Array.Copy(array, 0, Values, IndexWrite,array.Length);
            IndexWrite += array.Length;
            IndexBoolWrite = 0;
        }
        public void Write(bool value)
        {
            string cells = Convert.ToString(Values[IndexWrite], 2).PadLeft(8, '0');
            bool cell = (cells[IndexBoolWrite] == '1');
            if (cell != value)
            {
                byte add = (byte)Math.Pow(2, IndexBoolWrite);
                if (value)
                {
                    Values[IndexWrite] += add;
                }
                else
                {
                    Values[IndexWrite] -= add;
                }
            }
            IndexBoolWrite++;
            if (IndexBoolWrite == 8)
            {
                IndexBoolWrite = 0;
                IndexWrite++;
            }
        }
        public void Write(int value)
        {
            byte[] array = BitConverter.GetBytes(value);
            Write(array, false);
        }
        public void Write(char value)
        {
            byte[] charBytes = Encoding.Unicode.GetBytes(new[] { value }, 0, 1);
            Values[IndexWrite] = charBytes[0];
            IndexWrite++;
            Values[IndexWrite] = charBytes[1];
            IndexWrite++;
            IndexBoolWrite = 0;
        }
        public void Write(string array)
        {
            Write(array.Length);
            foreach (char item in array)
            {
                Write(item);
            }
            IndexBoolWrite = 0;
        }
        public void Write(DateTime value)
        {
            byte[] array = BitConverter.GetBytes(value.Ticks);
            Write(array, false);
        }

        public byte ReadByte()
        {
            byte ret = Values[IndexRead];
            IndexRead++;
            IndexBoolRead = 0;
            return ret;
        }
        public byte[] ReadByteArray()
        {
            int Length = ReadInt();
            byte[] ret = ReadByteArray(Length);
            return ret;
        }
        public byte[] ReadByteArray(int Length)
        {
            byte[] ret = new byte[Length];
            for (int i = 0; i < Length; i++)
            {
                ret[i] = Values[IndexRead];
                IndexRead++;
            }
            IndexBoolRead = 0;
            return ret;
        }
        public bool ReadBool()
        {
            string cells = Convert.ToString(Values[IndexRead], 2).PadLeft(8, '0');
            bool ret = (cells[cells.Length - IndexBoolRead - 1] == '1');
            IndexBoolRead++;
            if (IndexBoolRead == 8)
            {
                IndexBoolRead = 0;
                IndexRead++;
            }
            return ret;
        }
        public int ReadInt()
        {
            byte[] array = ReadByteArray(4);
            int ret = BitConverter.ToInt32(array, 0);
            return ret;
        }
        public char ReadChar()
        {
            byte[] arr = { ReadByte(), ReadByte() };
            char ret = Encoding.Unicode.GetChars(arr)[0];
            return ret;
        }
        public string ReadString()
        {
            int Length = ReadInt();
            string ret = "";
            for (int i = 0; i < Length; i++)
            {
                ret += ReadChar();
            }
            return ret;
        }
        public DateTime ReadTime()
        {
            byte[] array = ReadByteArray(8);
            long ticks = BitConverter.ToInt64(array, 0);
            DateTime ret = new DateTime(ticks);
            return ret;
        }
    }
}