﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.IO;

namespace VoteDLL
{
    class ItemShow
    {
        static public Color[] ColorTrue = { Color.Blue, Color.Orange, Color.Green };
        static public Color[] ColorFalse = { Color.Red, Color.Gray, Color.Yellow};
        static public int Checks = 0;
        static public int imageChangeTiks = 5000;

        static public ItemShow[] Selects = new ItemShow[3];

        public ItemData item;

        public Panel panle;
        public Label Name;
        public Label ID;
        public Label Points;
        public PictureBox image;
        public Bitmap[] imageList;

        public int imageNow = -1;
        public Timer imagesShange;
        private bool check = false;
        public bool Check
        {
            get
            {
                return check;
            }
            set
            {
                check = value;
                panle.BackColor = (check) ? ColorTrue[item.Project] : ColorFalse[item.Project];
                if (check)
                    Checks++;
                else
                    Checks--;
            }
        }

        public ItemShow(ItemData item, bool IsPanle)
        {
            panle = new Panel();
            panle.Location = Point.Empty;
            panle.Size = new Size(140, 240);
            panle.BackColor = ColorFalse[item.Project];
            Name = new Label();
            Name.Location = new Point(40, 10);
            ID = new Label();
            ID.Location = new Point(40, 30);
            Points = new Label();
            Points.Location = new Point(40, 50);
            image = new PictureBox();
            image.Location = new Point(20, 100);
            image.Size = new Size(100, 120);
            imagesShange = new Timer();
            imagesShange.Interval = imageChangeTiks;
            SetDate(item);

            panle.Click += Click;
            Name.Click += Click;
            ID.Click += Click;
            Points.Click += Click;
            image.Click += Click;
            imagesShange.Tick += Tick;

            panle.Controls.Add(Name);
            panle.Controls.Add(ID);
            panle.Controls.Add(Points);
            panle.Controls.Add(image);
            imagesShange.Start();
        }

        public void SetDate(ItemData item)
        {
            this.item = item;
            Name.Text = "Name: " + item.Name;
            ID.Text = "ID: " + item.ID;
            Points.Text = "Points: " + item.Points;

            if (item.ImageBytes.Length > 0)
            {
                if (item.ImageBytes.Length == 1)
                {
                    string filename = Files.UrlImages + @"\";
                    filename += item.Image + ".png";
                    try
                    {
                        File.WriteAllBytes(filename, item.ImageBytes[0]);
                    }
                    catch { }
                    imageList = new Bitmap[] { new Bitmap(filename) };
                    image.Image = imageList[0];
                }
                else
                {
                    imageNow = 0;
                    imageList = new Bitmap[item.ImageBytes.Length];
                    Directory.CreateDirectory(Files.UrlImages + "\\" + item.Image);
                    for (int i = 0; i < item.ImageBytes.Length; i++)
                    {
                        string filename = Files.UrlImages + "\\";
                        filename += item.Image + "\\" + (i + 1) + ".png";
                        try
                        {
                            File.WriteAllBytes(filename, item.ImageBytes[i]);
                        }
                        catch { }
                        imageList[i] = new Bitmap(filename);
                    }
                    image.Image = imageList[0];
                }
            }
        }

        public void AddPoint(int point)
        {
            item.AddPoint(point);
            Points.Text = "Points: " + item.Points;
        }

        private void Click(object s, EventArgs e)
        {
            if (!Check)
            {
                if (Selects[item.Project] == null)
                {
                    Selects[item.Project] = this;
                    Check = true;
                }
            }
            else
            {
                if (Selects[item.Project] == this)
                {
                    Selects[item.Project] = null;
                    Check = false;
                }
            }
        }
        private void Tick(object s, EventArgs e)
        {
            if (imageNow != -1)
            {
                imageNow++;
                if (imageNow == imageList.Length)
                    imageNow = 0;
                image.Image = imageList[imageNow];
            }
        }
    }
}
