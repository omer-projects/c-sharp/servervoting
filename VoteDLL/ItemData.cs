﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoteDLL
{
    public class ItemData
    {
        public string ID;
        public string Name;
        public string Image;
        public byte[][] ImageBytes;
        public int Project = 0;

        public int Points = 0;

        public ItemData()
        {

        }
        public ItemData(string line)
        {
            string[] split = line.Split(':');
            try
            {
                Project = int.Parse(split[0]);
            }
            catch
            {
                Project = 0;
            }

            ID = (split.Length > 1) ? split[1] : "";
            Name = (split.Length > 2) ? split[2] : "";
            Image = (split.Length > 3) ? split[3] : "";
        }
        public ItemData(ByteArray byteArray)
        {
            Project = byteArray.ReadInt();
            ID = byteArray.ReadString();
            Name = byteArray.ReadString();
            Image = byteArray.ReadString();
            ImageBytes = new byte[byteArray.ReadInt()][];
            for (int i = 0; i < ImageBytes.Length; i++)
            {
                ImageBytes[i] = byteArray.ReadByteArray();
            }
            Points = byteArray.ReadInt();
        }

        public void AddPoint(int point)
        {
            Points += point;
        }
    }
}
