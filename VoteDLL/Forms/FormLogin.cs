﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VoteDLL
{
    public partial class FormLogin : Form
    {
        public FormLogin()
        {
            InitializeComponent();
        }

        public Client client;
        public bool IsConnect = false;

        private void Read(byte[] array)
        {
            if (InvokeRequired)
            {
                Invoke(new EventByte(Read), array);
                return;
            }
            ByteArray command = new ByteArray(array);
            char hard = command.ReadChar();
            if (hard == 'C')
            {
                bool IsGood = ('T' == command.ReadChar());
                if (IsGood)
                {
                    MessageBox.Show("Good");
                    IsConnect = true;
                    Close();
                }
                else
                {
                    ByteArray byteArray = new ByteArray();
                    byteArray.Write('E');
                    client.Write(byteArray.Values);
                    System.Threading.Thread.Sleep(1000);
                    client.Disconnect();
                    client = null;
                    MessageBox.Show("The Password Not Good!!!");
                }
            }
        }

        private void buttonlogIn_Click(object sender, EventArgs e)
        {
            if (client != null)
                return;
            client = new Client(Net.Port, Net.MaxByte);
            client.Read = Read;
            client.Connect(BoxIP.Text);
            ByteArray byteArray = new ByteArray();
            byteArray.Write('C');
            byteArray.Write(BoxPassword.Text);
            client.Write(byteArray.Values);
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
