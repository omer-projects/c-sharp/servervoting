﻿namespace VoteDLL
{
    partial class FormLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonExit = new System.Windows.Forms.Button();
            this.timerChat = new System.Windows.Forms.Timer(this.components);
            this.BoxIP = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.BoxPassword = new System.Windows.Forms.TextBox();
            this.buttonlogIn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonExit
            // 
            this.buttonExit.Location = new System.Drawing.Point(51, 140);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(93, 50);
            this.buttonExit.TabIndex = 0;
            this.buttonExit.Text = "Exit";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // timerChat
            // 
            this.timerChat.Interval = 1;
            // 
            // BoxIP
            // 
            this.BoxIP.Location = new System.Drawing.Point(74, 55);
            this.BoxIP.Name = "BoxIP";
            this.BoxIP.Size = new System.Drawing.Size(169, 20);
            this.BoxIP.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "IP:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Password:";
            // 
            // BoxPassword
            // 
            this.BoxPassword.Location = new System.Drawing.Point(74, 90);
            this.BoxPassword.Name = "BoxPassword";
            this.BoxPassword.Size = new System.Drawing.Size(169, 20);
            this.BoxPassword.TabIndex = 3;
            // 
            // buttonlogIn
            // 
            this.buttonlogIn.Location = new System.Drawing.Point(162, 140);
            this.buttonlogIn.Name = "buttonlogIn";
            this.buttonlogIn.Size = new System.Drawing.Size(93, 50);
            this.buttonlogIn.TabIndex = 5;
            this.buttonlogIn.Text = "Log In";
            this.buttonlogIn.UseVisualStyleBackColor = true;
            this.buttonlogIn.Click += new System.EventHandler(this.buttonlogIn_Click);
            // 
            // FormLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 207);
            this.Controls.Add(this.buttonlogIn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.BoxPassword);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BoxIP);
            this.Controls.Add(this.buttonExit);
            this.Name = "FormLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Log In";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Timer timerChat;
        private System.Windows.Forms.TextBox BoxIP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox BoxPassword;
        private System.Windows.Forms.Button buttonlogIn;
    }
}

