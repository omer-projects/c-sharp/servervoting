﻿namespace VoteDLL
{
    partial class FormVoter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PanelTop = new System.Windows.Forms.Panel();
            this.BoxKey = new System.Windows.Forms.RichTextBox();
            this.buttonSand = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.ItemsShow = new System.Windows.Forms.FlowLayoutPanel();
            this.PanelTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // PanelTop
            // 
            this.PanelTop.Controls.Add(this.BoxKey);
            this.PanelTop.Controls.Add(this.buttonSand);
            this.PanelTop.Controls.Add(this.label1);
            this.PanelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelTop.Location = new System.Drawing.Point(0, 0);
            this.PanelTop.Name = "PanelTop";
            this.PanelTop.Size = new System.Drawing.Size(1000, 100);
            this.PanelTop.TabIndex = 0;
            // 
            // BoxKey
            // 
            this.BoxKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.BoxKey.Location = new System.Drawing.Point(429, 28);
            this.BoxKey.Name = "BoxKey";
            this.BoxKey.Size = new System.Drawing.Size(183, 50);
            this.BoxKey.TabIndex = 2;
            this.BoxKey.Text = "";
            this.BoxKey.TextChanged += new System.EventHandler(this.BoxKey_TextChanged);
            // 
            // buttonSand
            // 
            this.buttonSand.Location = new System.Drawing.Point(628, 23);
            this.buttonSand.Name = "buttonSand";
            this.buttonSand.Size = new System.Drawing.Size(75, 50);
            this.buttonSand.TabIndex = 1;
            this.buttonSand.Text = "Sand";
            this.buttonSand.UseVisualStyleBackColor = true;
            this.buttonSand.Click += new System.EventHandler(this.buttonSand_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label1.Location = new System.Drawing.Point(376, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Key:";
            // 
            // ItemsShow
            // 
            this.ItemsShow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ItemsShow.Location = new System.Drawing.Point(0, 100);
            this.ItemsShow.Name = "ItemsShow";
            this.ItemsShow.Size = new System.Drawing.Size(1000, 133);
            this.ItemsShow.TabIndex = 1;
            // 
            // FormVoter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 233);
            this.Controls.Add(this.ItemsShow);
            this.Controls.Add(this.PanelTop);
            this.Name = "FormVoter";
            this.Text = "FormVoter";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormVoter_FormClosed);
            this.Load += new System.EventHandler(this.FormVoter_Load);
            this.PanelTop.ResumeLayout(false);
            this.PanelTop.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelTop;
        private System.Windows.Forms.RichTextBox BoxKey;
        private System.Windows.Forms.Button buttonSand;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel ItemsShow;
    }
}