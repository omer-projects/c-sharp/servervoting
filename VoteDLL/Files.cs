﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace VoteDLL
{
    static public class Files
    {
        static public string UrlImages = @"\Images";

        static public void Load()
        {
            UrlImages = Application.StartupPath + UrlImages;
            if (!Directory.Exists(UrlImages))
            {
                Directory.CreateDirectory(UrlImages);
            }
            else
            {
                string[] folders = Directory.GetDirectories(UrlImages);
                foreach (var item in folders)
                {
                    try
                    {
                        string[] filesIn = Directory.GetFiles(item);
                        foreach (var itemIn in filesIn)
                        {
                            File.Delete(item);
                        }
                        Directory.Delete(item);
                    }
                    catch { };
                }
                string[] files = Directory.GetFiles(UrlImages);
                foreach (var item in files)
                {
                    try
                    {
                        File.Delete(item);
                    }
                    catch { };
                }
            }
        }
    }
}
